# Assignment 5 - Image Processing
A program that uses restclient to fetch a URL to an image, downloads the image and creates a modified version of it using opencv. 

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `gcc`, `cmake`, `VSCode`, `restclient-cpp`, `nlohmann/json`, `opencv`.

## Setup

Clone the git repo and start VSCode in the processingimages root folder. Use the following instructions to build and run:

    ```sh
    # Press F1 and select CMake:Build.
    $ ./build/assignment5
    ```
The naming convention is such that the original image is saved as cat+"some number".jpg in the original foler and the output is saved as test+"corresponding number".jpg in the output folder.

## Maintainers

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

## Contributors and License
Copyright 2022,

[Hannes Ringblom @HannesRingblom](https://gitlab.com/HannesRingblom)
[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)
