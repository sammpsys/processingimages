#include <iostream>
#include <opencv2/opencv.hpp>
#include "restclient-cpp/restclient.h"
#include "restclient-cpp/connection.h"
#include <nlohmann/json.hpp>
#include <cstdlib>
#include <string>
#include <cstring>
using json = nlohmann::json;
int MAX_KERNEL_LENGTH=31;           //arbitrary kernel length for grayscale
bool imgsize=false;                 //boolean flag for image reduction


int main(int, char**) {

    // initialize RestClient
    RestClient::init();

    // get a connection object
    start:
        RestClient::Response r = RestClient::get("https://aws.random.cat/meow");
        auto json_body = json::parse(r.body);
        std::string img_url=json_body["file"];          //url 
        if (img_url.back()=='f'){                       //ensures that a gif is not downloaded
            std::cout<<"Found gif, try new"<<std::endl; 
            goto start;                                 //cycles back to start of request
        }
    std::vector<std::string> filenames;                 //vector storing files in original directory

    
    cv::glob("original/*.jpg", filenames);              // Get all jpg in the folder
    int number=filenames.size();                        // save jpg as cat+number.jpg
    std::string inputstring="wget -P original -A jpg " +img_url + " -O original/cat"+std::to_string(number)+".jpg";
    system(inputstring.c_str());                        //wget request

    cv::Mat image = cv::imread("original/cat"+std::to_string(number)+".jpg"); //save original as image object
    cv::Mat outimage;                               //preparing output images as image objects
    cv::Mat outimage2;
    std::string outname="output/test"+std::to_string(number)+".jpg";  //preparing output file as test+number.jpg
    while(!imgsize){                                                    //perform image resizing while preserving aspect ratio
        if(image.cols>300 || image.rows>300){                           
            resize(image,image, cv::Size(), 0.9, 0.9, cv::INTER_LINEAR);
        }
        else{
            imgsize=true;
        }
    }

    cv::cvtColor(image, outimage, cv::COLOR_BGR2GRAY ); // Convert the image to Gray
    for ( int j = 1; j < MAX_KERNEL_LENGTH; j = j + 2 )     //perform medianblur
        {
            cv::medianBlur ( outimage, outimage2, j );
        }
    bool check=cv::imwrite(outname,outimage2);          //save changed image as name
}
